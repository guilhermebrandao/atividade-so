package com.brandao.guilherme.atividade_rao_mutex;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.jraska.console.Console;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.Semaphore;

public class MainActivity extends AppCompatActivity {

    Button mButton;
    EditText mQtdProcessos;
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);

    public static final Integer APENAS_UM_PROCESSO_POR_VEZ = 1;
    public static final String DATE_PATTERN = "dd/MM/aaaa HH:mm:ss";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_main);


        mButton = findViewById(R.id.button);
        mQtdProcessos = findViewById(R.id.editText_qtd_processos);

        mButton.setOnClickListener(mButtonClick);

        Console.write(dateFormat.format(Calendar.getInstance().getTime()) + ":" + " Insira a quantidade de processo e aperte Iniciar\n");

    }

    View.OnClickListener mButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!TextUtils.isEmpty(mQtdProcessos.getText())) {
                rodarThread(Integer.valueOf(mQtdProcessos.getText().toString()));
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Alerta");
                alertDialog.setMessage("Digite só números ai, vá");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }

        }
    };

    void rodarThread(final int qtdProcessos) {
        int numeroDeProcessos = qtdProcessos;
        Semaphore semaphore = new Semaphore(APENAS_UM_PROCESSO_POR_VEZ);
        ProcessadorThread[] processos = new ProcessadorThread[numeroDeProcessos];
        for (int i = 0; i < numeroDeProcessos; i++) {
            processos[i] = new ProcessadorThread(i, semaphore);
            processos[i].start();
        }
    }

    public class ProcessadorThread extends Thread {
        private int idThread;
        private Semaphore semaforo;

        public ProcessadorThread(int id, Semaphore semaphore) {
            this.idThread = id;
            this.semaforo = semaphore;
        }

        /**
         * ...
         */

        /**
         * Faz a thread dormir por algum tempo, simulando o efeito de um processamento longo.
         */
        private void processar() {

            try {
                Console.write("\nThread #" + idThread + " processando");
                Thread.sleep((long) (Math.random() * 10000));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Este método simula o acesso da Thread em uma região não crítica, ou seja, uma região
         * ao qual não é necessário pedir uma trava. Exibimos o atual estado da Thread, para facilitar
         * o entendimento do progama, e realizamos um processamento qualquer.
         */
        private void entrarRegiaoNaoCritica() {

            Console.write("\nThread #" + idThread + " em região não crítica");
            processar();
        }

        /**
         * Este outro método será utilizado para simular o acesso da Thread em uma região crítica.
         * Ele será chamado logo após conseguir a trava do semáforo.
         */
        private void entrarRegiaoCritica() {

            Console.write("\nThread #" + idThread
                    + " entrando em região crítica");
            processar();
            Console.write("\nThread #" + idThread + " saindo da região crítica");
        }

        /**
         * Neste método nós realizamos um processamento não crítico, depois requisitamos o acesso
         * ao semáforo (com o semaforo.acquire()) e em seguida realizamos o processamento de uma
         * região crítica. Por fim, liberamos o recurso do semáforo (com o semaforo.release()).
         */
        public void run() {
            entrarRegiaoNaoCritica();
            try {
                semaforo.acquire();
                entrarRegiaoCritica();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                semaforo.release();
            }
        }

    }

}

